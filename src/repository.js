const { ObjectID } = require("mongodb");

module.exports = {
  mongoRepository(db) {
    const collection = db.collection("notes");
    const module = {};

    // todo: don't depend on mongo response, create custom interface

    module.create = function create({ title, description }) {
      return collection
        .insertOne({ title, description })
        .then(({ insertedId }) => {
          return {
            _id: insertedId,
            title,
            description,
          };
        });
    };

    module.getAll = function getAll() {
      return collection.find().toArray();
    };

    module.get = function get(id) {
      return collection.findOne({ _id: ObjectID(id) });
    };

    module.update = function update({ id, title, description }) {
      return collection
        .updateOne({ _id: ObjectID(id) }, { $set: { title, description } })
        .then(({ result }) => {
          if (result.n === 0) {
            return null;
          }

          return { _id: id, title, description };
        });
    };

    module.delete = function _delete(id) {
      return collection.deleteOne({ _id: ObjectID(id) }).then(({ result }) => {
        return result.n === 1;
      });
    };

    return module;
  },
};
