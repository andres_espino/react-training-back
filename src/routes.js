const express = require("express");

module.exports = {
  notesRouter(controller) {
    const router = express.Router();

    router.post("/", async (req, res) => {
      try {
        const result = await controller.create(req.body);
        res.json(result).end();
      } catch (e) {
        return res.status(400).send(e.message).end();
      }
    });

    router.get("/", async (req, res) => {
      try {
        const result = await controller.getAll();
        res.json(result).end();
      } catch (e) {
        return res.status(400).send(e.message).end();
      }
    });

    router.get("/:id", async (req, res) => {
      try {
        const result = await controller.getById(req.params.id);
        res.json(result).end();
      } catch (e) {
        return res.status(400).send(e.message).end();
      }
    });

    router.put("/:id", async (req, res) => {
      try {
        const result = await controller.update({
          id: req.params.id,
          ...req.body,
        });
        if (result === null) {
          res.sendStatus(404).end();
        } else {
          res.json(result).end();
        }
      } catch (e) {
        return res.status(400).send(e.message).end();
      }
    });

    router.delete("/:id", async (req, res) => {
      try {
        const result = await controller.delete(req.params.id);
        const status = result ? 204 : 404;
        res.sendStatus(status).end();
      } catch (e) {
        return res.status(400).send(e.message).end();
      }
    });

    return router;
  },
};
