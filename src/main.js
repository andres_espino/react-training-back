const express = require("express");
const { MongoClient } = require("mongodb");

const { notesRouter } = require("./routes");
const { notesController } = require("./controller");
const { mongoRepository } = require("./repository");

const MONGO_CONN_URI =
  process.env.MONGODB_CONNECTION_URI || "mongodb://localhost:27017";
const PORT = process.env.port || 3000;

const app = express();

(async function init() {
  const dbClient = MongoClient(MONGO_CONN_URI, {
    useUnifiedTopology: true,
  });
  await dbClient.connect();
  const db = await dbClient.db("notes");

  const repository = mongoRepository(db);
  const controller = notesController(repository);
  const router = notesRouter(controller);

  app.use(express.json());
  app.use((req, res, next) => {
    console.log("request receive: ", req.method, req.body);
    next();
  });
  app.use("/notes", router);
  app.use(function (err, req, res, next) {
    console.error(err.stack);
    res.status(500).send("Something broke!");
  });
  app.listen(PORT);

  console.log(`App running on http://localhost:${PORT}`);
})();
