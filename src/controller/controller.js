const escapeHtml = require("escape-html");
const { isValidNote } = require("./validator");

module.exports = {
  notesController(repository) {
    const module = {};

    module.create = function create({ title = "", description = "" }) {
      if (isValidNote({ title, description })) {
        return repository.create({
          title: escapeHtml(title),
          description: escapeHtml(description),
        });
      } else {
        return Promise.reject("Invalid note");
      }
    };

    module.getAll = function getAll() {
      return repository.getAll();
    };

    module.getById = function getById(id) {
      return repository.get(id);
    };

    module.update = function update({ id, title = "", description = "" }) {
      if (isValidNote({ title, description })) {
        return repository.update({
          id,
          title: escapeHtml(title),
          description: escapeHtml(description),
        });
      } else {
        return Promise.reject("invalid note");
      }
    };

    module.delete = function _delete(id) {
      return repository.delete(id);
    };

    return module;
  },
};
