module.exports = {
  isValidNote({ title, description }) {
    return (
      typeof title === "string" &&
      typeof description === "string" &&
      title.length > 3
    );
  },
};
